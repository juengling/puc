'''
Created on 22.12.2015

@author: chris
'''

class FileInfo(object):

    def __init__(self, name):
        self._name = name
        self._version = '0.0'
        self._status = ''
        self._downloadurl = ''
        
    @property
    def name(self):
        return self._name
    
    @property
    def version(self):
        return self._version

    @version.setter
    def version(self, value):
        self._version = value
        
    @property
    def status(self):
        return self._status
    
    @status.setter
    def status(self, value):
        if value in ('alpha', 'beta', 'final'):
            self._status = value
        else:
            raise Exception('Illegal value: {0}'.format(value))
    
    @property
    def downloadurl(self):
        return self._downloadurl
    
    @downloadurl.setter
    def downloadurl(self, value):
        self._downloadurl = value
