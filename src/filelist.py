'''
Created on 20.12.2015

@author: chris
'''

import xml.etree.ElementTree as ET
from fileinfo import FileInfo
import logging


class FileList(object):

    def __init__(self, filedata):
        self._logger = logging.getLogger('FileList')
        self._listoffiles = []
        root = ET.fromstring(filedata)
        currentfile = None
        for child in root:
            # Iterate through the file list
            if child.tag == 'file':
                if currentfile:
                    self._listoffiles.append(currentfile)
                    
                currentfile = FileInfo(child.attrib['label'])
                self._logger.debug('Program: %s', child.attrib['label'])
                self._logger.debug('Check every: %s', child.attrib['interval'])
                
                for att in child:
                    if att.tag == 'installed-version':
                        currentfile.version = att.text
                        self._logger.debug('installed-version: %s', att.text)
                    elif att.tag == 'check-url':
                        currentfile.downloadurl = att.text
                        self._logger.debug('check-url: %s', att.text)
            else:
                print('Unknown xml tag: {0}'.format(child.tag))
                
        if currentfile:
            self._listoffiles.append(currentfile)

    def getfileinfo(self):
        return self._listoffiles
