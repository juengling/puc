'''
Created on 19.12.2015

@author: chris
'''

from argparse import ArgumentParser
import logging
from os import path, getcwd, getlogin
import platform
import sys
from filelist import FileList


__myname__ = 'puc'
__myshortname__ = 'puc'
__title__ = 'Python Update Checker by Christoph Juengling'
__version__ = '0.1.0'

__return_ok__ = 0

def main():
    args = ParseCommandLine()
    
    # Init logging
    if args.debug:
        logginglevel = logging.DEBUG
    else:
        logginglevel = logging.INFO
    
    if args.log:
        # Optional use default log file
        print(getlogfilename())
        logging.basicConfig(filename=fullpath(args.log), format='%(asctime)s; %(levelname)s; %(name)s; %(message)s', level=logginglevel)
    else:
        logging.basicConfig(level=logginglevel)
        
    logger = logging.getLogger(__myname__)
    
    try:
        logger.debug('Start %s v%s', __myname__, __version__)

        if args.version:
            logger.debug('Print version number and exit')
            print('{0:s} v{1:s}\n{2}'.format(__myname__, __version__, __title__))
            return __return_ok__
        
        path = getsettingsfile('puc-files.xml')
        
        
        with open (path, 'r') as myfile:
            filelist = FileList(myfile.read())
        
        for f in filelist.getfileinfo():
            print(f.name)
            
    except Exception:
        logger.exception('Error')
        return 2
    
    logger.debug('Finished.')
    
    #wp.call(GetUserInfo())


def ParseCommandLine():
    parser = ArgumentParser(prog=__title__ + ' v' + __version__, description=main.__doc__)
    
    #parser.add_argument('--downloadarticles', action='store_true', help='Read articles from remote')
    #parser.add_argument('--storearticles', action='store_true', help='Store articles into local files')
    parser.add_argument('-v', '--version', action='store_true', help='Print version information and exit')
    parser.add_argument('--debug', action='store_true', help='Print debug information during work')
    parser.add_argument('-l', '--log', help='Name or path for the logfile (console is used, if not specified)')

    args = parser.parse_args()
    return(args)


def getsettingsfile(filename):
    '''
    Determine default settings file location on several operating systems
    '''
    if platform.system() == 'Linux':
        return(path.expanduser('~/.{0:s}/{1:s}'.format(__myname__, filename)))
    elif platform.system() == 'Windows':
        return('C:\\Users\\{0:s}\\AppData\Local\\{1:s}\\{2:s}'.format(getlogin(), __myname__, filename))


def getlogfilename():
    '''
    Determine default log file location on several operating systems
    '''
    if platform.system() == 'Linux':
        return('/var/log/{0:s}.log'.format(__myname__))
    elif platform.system() == 'Windows':
        return('C:\\Users\\{0:s}\\AppData\Local\\{1:s}.log'.format(getlogin(), __myname__))


def fullpath(filename):
    if (path.dirname(filename) == ''):
        # If no path exists, add current work directory
        return (path.join(getcwd(), filename))
    else:
        return (filename)

if __name__ == '__main__':
    sys.exit(main())
