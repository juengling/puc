'''
Created on 22.12.2015

@author: chris
'''

import unittest
from filelist import FileList
from fileinfo import FileInfo


class Test(unittest.TestCase):


    def setUp(self):
        self._filedata = '''
<puc>
    <file label="puc" interval="1w">
        <installed-version>0.1.0</installed-version>
        <check-url>https://bitbucket.org/juengling/puc/downloads/info.xml</check-url>
    </file>
</puc>
'''


    def tearDown(self):
        pass


    def testReadFileList(self):
        fl = FileList(self._filedata)
        actual = fl.getfileinfo()
        self.assertEqual(1, len(actual), 'Must have exactly one list element')
        self.assertEqual(actual[0].name, 'puc', 'Name is not equal')
        self.assertEqual(actual[0].version, '0.1.0', 'Version is not equal')
        self.assertEqual(actual[0].status, '', 'Status is not equal')
        self.assertEqual(actual[0].downloadurl, 'https://bitbucket.org/juengling/puc/downloads/info.xml', 'URL is not equal')

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()